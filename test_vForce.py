#!/usr/bin/env python
from __future__ import print_function, division
import math
from libvecLib import ffi, libvecLib

def test_vForce_vvsin():
    # https://developer.apple.com/library/mac/documentation/Performance/Conceptual/vecLib/#//apple_ref/c/func/vvsin
    # vvsin(double *out, const double *in, const int* N)

    # Allocate and initialise array
    N = 16 
    a = ffi.new("double[]", N)
    for i in range(N):
        a[i] = i
    print("A in ", [x for x in a])

    # sin()
    Np = ffi.new("int *", N)
    libvecLib.vvsin(a, a, Np)
    print("A out", [x for x in a])

    # assert output array matches expected answer
    expected = [math.sin(i) for i in range(N)]
    assert all(abs(a-e) < 1e-6 for a, e in zip(a, expected))


def test_vForce_vvdiv():
    # Allocate and initialise array
    N = 16
    a = ffi.new("double[]", N)
    b = ffi.new("double[]", N)
    c = ffi.new("double[]", N)
    for i in range(N):
        a[i] = (i+1) * 2
        b[i] = (i+1)
    print("A in ", [x for x in a])
    print("B in ", [x for x in b])
    print("C in ", [x for x in c])

    Np = ffi.new("int *", N)
    libvecLib.vvdiv(c, a, b, Np)
    print("C out", [x for x in c])

    # assert output array matches expected answer
    expected = [2 for i in range(N)]
    assert all(abs(a-e) < 1e-6 for a, e in zip(c, expected))


def test_vForce_inplace_vvdiv():
    # Allocate and initialise array
    N = 16
    a = ffi.new("double[]", N)
    b = ffi.new("double[]", N)
    for i in range(N):
        a[i] = (i+1) * 2
        b[i] = (i+1)
    print("A in ", [x for x in a])
    print("B in ", [x for x in b])

    Np = ffi.new("int *", N)
    libvecLib.vvdiv(a, a, b, Np)
    print("A out", [x for x in a])

    # assert output array matches expected answer
    expected = [2 for i in range(N)]
    assert all(abs(a-e) < 1e-6 for a, e in zip(a, expected))
