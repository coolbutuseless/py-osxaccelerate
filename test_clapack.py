#!/usr/bin/env python
from __future__ import print_function, division
from array import array
from libvecLib import libvecLib, ffi

# Example translated from C code from intel:
# https://software.intel.com/sites/products/documentation/doclib/mkl_sa/11/mkl_lapack_examples/dgels_ex.c.htm

def get_double_pointer_from_array(x):
    return ffi.cast("double *", x.buffer_info()[0])

def test_clapack_dgels():
    a = array('d', [
                1.44, -9.96, -7.55,  8.34,  7.08, -5.45,
               -7.84, -0.28,  3.24,  8.09,  2.52, -5.70,
               -4.39, -3.24,  6.27,  5.28,  0.74, -1.19,
                4.53,  3.83, -6.64,  2.06, -2.47,  4.70])

    b = array('d', [
                8.58,  8.26,  8.48, -5.28,  5.72,  8.93,
                9.35, -4.43, -0.70, -0.26, -7.36, -2.52])

    # Answers. Run clapack/dgels_ex to confirm correct output from C code
    x0 = [-0.45, -0.85, 0.71, 0.13]
    x1 = [ 0.25, -0.90, 0.63, 0.14]

    worklength = 100 # scratch space used in LAPACK
    trans = ffi.new("char *", b"N") # N = no transpose, T=transpose
    m     = ffi.new("int *", 6) # Nrows of A
    n     = ffi.new("int *", 4) # Ncols of A
    nrhs  = ffi.new("int *", 2) # Ncols of B i.e. number of RHSs to solve for
    apointer = get_double_pointer_from_array(a)
    lda   = ffi.new("int *", 6)
    bpointer = get_double_pointer_from_array(b)
    ldb   = ffi.new("int *", 6)
    work = ffi.new("double[]", worklength)
    lwork = ffi.new("int *", worklength)
    info  = ffi.new("int *", 999)

    # dgels description: http://www.netlib.org/lapack/lug/node27.html
    # dgels file/arguments reference: http://www.netlib.org/lapack/explore-html/d8/dde/dgels_8f.html
    #
    # OSX Accelerate LAPACK uses fortran interface. Assumes column-major ordering
    #
    # LAPACK functions for dgels - all arguments are pointers
    # int dgels_(char *__trans, __CLPK_integer *__m, __CLPK_integer *__n,
    #         __CLPK_integer *__nrhs, __CLPK_doublereal *__a, __CLPK_integer *__lda,
    #         __CLPK_doublereal *__b, __CLPK_integer *__ldb,
    #         __CLPK_doublereal *__work, __CLPK_integer *__lwork,
    #         __CLPK_integer *__info)

    # Query and allocate the optimal workspace
    lwork = ffi.new("int *", -1)
    retval = libvecLib.dgels_(trans, m, n, nrhs, apointer, lda, bpointer, ldb, work, lwork, info)
    print("Optimal size for 'work' array", int(work[0]))
    assert len(work) < int(work[0]), "Need to allocate more space for 'work' array."

    # Solve A*X = B */
    lwork[0] = len(work)
    retval = libvecLib.dgels_(trans, m, n, nrhs, apointer, lda, bpointer, ldb, work, lwork, info)

    # Check for the full rank
    if info[0] > 0:
        print("The diagonal element %i of the triangular factor " % info );
        print("of A is zero, so that A does not have full rank;\n" );
        print("the least squares solution could not be computed.\n" );
        raise Exception("Rank error")

    # Print least squares solution
    print("Least squares solution")
    for i in range(n[0]):
        print("%5.2f" % b[i], "%5.2f" % b[m[0]+i])

    for i in range(n[0]):
        assert abs(b[i]        - x0[i]) <= 0.01
        assert abs(b[m[0] + i] - x1[i]) <= 0.01


