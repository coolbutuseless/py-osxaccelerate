#!/usr/bin/env python
from __future__ import print_function
import os
from cffi import FFI
ffi = FFI()

this_dir = os.path.dirname(os.path.abspath(__file__)) + "/"
header_dir = this_dir + "cffi_headers/vecLib/"
#                name          cdef override?
header_files = [('cblas.h'    , False),
                ('clapack.h'  , True ),
                ('vectorOps.h', False),
                ('vForce.h'   , True )]

for header_file, override in header_files:
    with open(header_dir + header_file) as h:
        ffi.cdef(h.read(), override=override)

#ffi.cdef(open("cffi_headers/vecLib/cblas.h").read())
#ffi.cdef(open("cffi_headers/vecLib/clapack.h").read(), override=True) # multiple definitions of _ilaver(). Set override
#ffi.cdef(open("cffi_headers/vecLib/vectorOps.h").read())
#ffi.cdef(open("cffi_headers/vecLib/vForce.h").read())

vecLib_libdir = "/System/Library/Frameworks/Accelerate.framework/Frameworks/vecLib.framework/Versions/Current/"
libvecLib = ffi.dlopen(vecLib_libdir + "vecLib")

