enum CBLAS_ORDER {CblasRowMajor=101, CblasColMajor=102 };
enum CBLAS_TRANSPOSE {CblasNoTrans=111, CblasTrans=112, CblasConjTrans=113,
 AtlasConj=114};
enum CBLAS_UPLO {CblasUpper=121, CblasLower=122};
enum CBLAS_DIAG {CblasNonUnit=131, CblasUnit=132};
enum CBLAS_SIDE {CblasLeft=141, CblasRight=142};
int cblas_errprn(int __ierr, int __info, char *__form, ...)
        ;
void cblas_xerbla(int __p, char *__rout, char *__form, ...)
        ;
float cblas_sdsdot(const int __N, const float __alpha, const float *__X,
        const int __incX, const float *__Y, const int __incY)
        ;
double cblas_dsdot(const int __N, const float *__X, const int __incX,
        const float *__Y, const int __incY)
        ;
float cblas_sdot(const int __N, const float *__X, const int __incX,
        const float *__Y, const int __incY)
        ;
double cblas_ddot(const int __N, const double *__X, const int __incX,
        const double *__Y, const int __incY)
        ;
void cblas_cdotu_sub(const int __N, const void *__X, const int __incX,
        const void *__Y, const int __incY, void *__dotu)
        ;
void cblas_cdotc_sub(const int __N, const void *__X, const int __incX,
        const void *__Y, const int __incY, void *__dotc)
        ;
void cblas_zdotu_sub(const int __N, const void *__X, const int __incX,
        const void *__Y, const int __incY, void *__dotu)
        ;
void cblas_zdotc_sub(const int __N, const void *__X, const int __incX,
        const void *__Y, const int __incY, void *__dotc)
        ;
float cblas_snrm2(const int __N, const float *__X, const int __incX)
        ;
float cblas_sasum(const int __N, const float *__X, const int __incX)
        ;
double cblas_dnrm2(const int __N, const double *__X, const int __incX)
        ;
double cblas_dasum(const int __N, const double *__X, const int __incX)
        ;
float cblas_scnrm2(const int __N, const void *__X, const int __incX)
        ;
float cblas_scasum(const int __N, const void *__X, const int __incX)
        ;
double cblas_dznrm2(const int __N, const void *__X, const int __incX)
        ;
double cblas_dzasum(const int __N, const void *__X, const int __incX)
        ;
int cblas_isamax(const int __N, const float *__X, const int __incX)
        ;
int cblas_idamax(const int __N, const double *__X, const int __incX)
        ;
int cblas_icamax(const int __N, const void *__X, const int __incX)
        ;
int cblas_izamax(const int __N, const void *__X, const int __incX)
        ;
void cblas_sswap(const int __N, float *__X, const int __incX, float *__Y,
        const int __incY) ;
void cblas_scopy(const int __N, const float *__X, const int __incX, float *__Y,
        const int __incY) ;
void cblas_saxpy(const int __N, const float __alpha, const float *__X,
        const int __incX, float *__Y, const int __incY)
        ;
void catlas_saxpby(const int __N, const float __alpha, const float *__X,
        const int __incX, const float __beta, float *__Y, const int __incY)
        ;
void catlas_sset(const int __N, const float __alpha, float *__X,
        const int __incX) ;
void cblas_dswap(const int __N, double *__X, const int __incX, double *__Y,
        const int __incY) ;
void cblas_dcopy(const int __N, const double *__X, const int __incX,
        double *__Y, const int __incY)
                     ;
void cblas_daxpy(const int __N, const double __alpha, const double *__X,
        const int __incX, double *__Y, const int __incY)
        ;
void catlas_daxpby(const int __N, const double __alpha, const double *__X,
        const int __incX, const double __beta, double *__Y, const int __incY)
        ;
void catlas_dset(const int __N, const double __alpha, double *__X,
        const int __incX) ;
void cblas_cswap(const int __N, void *__X, const int __incX, void *__Y,
        const int __incY) ;
void cblas_ccopy(const int __N, const void *__X, const int __incX, void *__Y,
        const int __incY) ;
void cblas_caxpy(const int __N, const void *__alpha, const void *__X,
        const int __incX, void *__Y, const int __incY)
        ;
void catlas_caxpby(const int __N, const void *__alpha, const void *__X,
        const int __incX, const void *__beta, void *__Y, const int __incY)
        ;
void catlas_cset(const int __N, const void *__alpha, void *__X,
        const int __incX) ;
void cblas_zswap(const int __N, void *__X, const int __incX, void *__Y,
        const int __incY) ;
void cblas_zcopy(const int __N, const void *__X, const int __incX, void *__Y,
        const int __incY) ;
void cblas_zaxpy(const int __N, const void *__alpha, const void *__X,
        const int __incX, void *__Y, const int __incY)
        ;
void catlas_zaxpby(const int __N, const void *__alpha, const void *__X,
        const int __incX, const void *__beta, void *__Y, const int __incY)
        ;
void catlas_zset(const int __N, const void *__alpha, void *__X,
        const int __incX) ;
void cblas_srotg(float *__a, float *__b, float *__c, float *__s)
        ;
void cblas_srotmg(float *__d1, float *__d2, float *__b1, const float __b2,
        float *__P) ;
void cblas_srot(const int __N, float *__X, const int __incX, float *__Y,
        const int __incY, const float __c, const float __s)
        ;
void cblas_srotm(const int __N, float *__X, const int __incX, float *__Y,
        const int __incY, const float *__P)
        ;
void cblas_drotg(double *__a, double *__b, double *__c, double *__s)
        ;
void cblas_drotmg(double *__d1, double *__d2, double *__b1, const double __b2,
        double *__P) ;
void cblas_drot(const int __N, double *__X, const int __incX, double *__Y,
        const int __incY, const double __c, const double __s)
        ;
void cblas_drotm(const int __N, double *__X, const int __incX, double *__Y,
        const int __incY, const double *__P)
        ;
void cblas_sscal(const int __N, const float __alpha, float *__X,
        const int __incX) ;
void cblas_dscal(const int __N, const double __alpha, double *__X,
        const int __incX) ;
void cblas_cscal(const int __N, const void *__alpha, void *__X,
        const int __incX) ;
void cblas_zscal(const int __N, const void *__alpha, void *__X,
        const int __incX) ;
void cblas_csscal(const int __N, const float __alpha, void *__X,
        const int __incX) ;
void cblas_zdscal(const int __N, const double __alpha, void *__X,
        const int __incX) ;
void cblas_crotg(void *__a, void *__b, void *__c, void *__s)
        ;
void cblas_zrotg(void *__a, void *__b, void *__c, void *__s)
        ;
void cblas_csrot(const int __N, void *__X, const int __incX, void *__Y,
        const int __incY, const float __c, const float __s)
        ;
void cblas_zdrot(const int __N, void *__X, const int __incX, void *__Y,
        const int __incY, const double __c, const double __s)
        ;
void cblas_sgemv(const enum CBLAS_ORDER __Order,
        const enum CBLAS_TRANSPOSE __TransA, const int __M, const int __N,
        const float __alpha, const float *__A, const int __lda,
        const float *__X, const int __incX, const float __beta, float *__Y,
        const int __incY) ;
void cblas_sgbmv(const enum CBLAS_ORDER __Order,
        const enum CBLAS_TRANSPOSE __TransA, const int __M, const int __N,
        const int __KL, const int __KU, const float __alpha, const float *__A,
        const int __lda, const float *__X, const int __incX,
        const float __beta, float *__Y, const int __incY)
        ;
void cblas_strmv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __TransA, const enum CBLAS_DIAG __Diag,
        const int __N, const float *__A, const int __lda, float *__X,
        const int __incX) ;
void cblas_stbmv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __TransA, const enum CBLAS_DIAG __Diag,
        const int __N, const int __K, const float *__A, const int __lda,
        float *__X, const int __incX)
                     ;
void cblas_stpmv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __TransA, const enum CBLAS_DIAG __Diag,
        const int __N, const float *__Ap, float *__X, const int __incX)
        ;
void cblas_strsv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __TransA, const enum CBLAS_DIAG __Diag,
        const int __N, const float *__A, const int __lda, float *__X,
        const int __incX) ;
void cblas_stbsv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __TransA, const enum CBLAS_DIAG __Diag,
        const int __N, const int __K, const float *__A, const int __lda,
        float *__X, const int __incX)
                     ;
void cblas_stpsv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __TransA, const enum CBLAS_DIAG __Diag,
        const int __N, const float *__Ap, float *__X, const int __incX)
        ;
void cblas_dgemv(const enum CBLAS_ORDER __Order,
        const enum CBLAS_TRANSPOSE __TransA, const int __M, const int __N,
        const double __alpha, const double *__A, const int __lda,
        const double *__X, const int __incX, const double __beta, double *__Y,
        const int __incY) ;
void cblas_dgbmv(const enum CBLAS_ORDER __Order,
        const enum CBLAS_TRANSPOSE __TransA, const int __M, const int __N,
        const int __KL, const int __KU, const double __alpha,
        const double *__A, const int __lda, const double *__X,
        const int __incX, const double __beta, double *__Y, const int __incY)
        ;
void cblas_dtrmv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __TransA, const enum CBLAS_DIAG __Diag,
        const int __N, const double *__A, const int __lda, double *__X,
        const int __incX) ;
void cblas_dtbmv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __TransA, const enum CBLAS_DIAG __Diag,
        const int __N, const int __K, const double *__A, const int __lda,
        double *__X, const int __incX)
                     ;
void cblas_dtpmv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __TransA, const enum CBLAS_DIAG __Diag,
        const int __N, const double *__Ap, double *__X, const int __incX)
        ;
void cblas_dtrsv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __TransA, const enum CBLAS_DIAG __Diag,
        const int __N, const double *__A, const int __lda, double *__X,
        const int __incX) ;
void cblas_dtbsv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __TransA, const enum CBLAS_DIAG __Diag,
        const int __N, const int __K, const double *__A, const int __lda,
        double *__X, const int __incX)
                     ;
void cblas_dtpsv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __TransA, const enum CBLAS_DIAG __Diag,
        const int __N, const double *__Ap, double *__X, const int __incX)
        ;
void cblas_cgemv(const enum CBLAS_ORDER __Order,
        const enum CBLAS_TRANSPOSE __TransA, const int __M, const int __N,
        const void *__alpha, const void *__A, const int __lda, const void *__X,
        const int __incX, const void *__beta, void *__Y, const int __incY)
        ;
void cblas_cgbmv(const enum CBLAS_ORDER __Order,
        const enum CBLAS_TRANSPOSE __TransA, const int __M, const int __N,
        const int __KL, const int __KU, const void *__alpha, const void *__A,
        const int __lda, const void *__X, const int __incX, const void *__beta,
        void *__Y, const int __incY)
                     ;
void cblas_ctrmv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __TransA, const enum CBLAS_DIAG __Diag,
        const int __N, const void *__A, const int __lda, void *__X,
        const int __incX) ;
void cblas_ctbmv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __TransA, const enum CBLAS_DIAG __Diag,
        const int __N, const int __K, const void *__A, const int __lda,
        void *__X, const int __incX)
                     ;
void cblas_ctpmv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __TransA, const enum CBLAS_DIAG __Diag,
        const int __N, const void *__Ap, void *__X, const int __incX)
        ;
void cblas_ctrsv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __TransA, const enum CBLAS_DIAG __Diag,
        const int __N, const void *__A, const int __lda, void *__X,
        const int __incX) ;
void cblas_ctbsv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __TransA, const enum CBLAS_DIAG __Diag,
        const int __N, const int __K, const void *__A, const int __lda,
        void *__X, const int __incX)
                     ;
void cblas_ctpsv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __TransA, const enum CBLAS_DIAG __Diag,
        const int __N, const void *__Ap, void *__X, const int __incX)
        ;
void cblas_zgemv(const enum CBLAS_ORDER __Order,
        const enum CBLAS_TRANSPOSE __TransA, const int __M, const int __N,
        const void *__alpha, const void *__A, const int __lda, const void *__X,
        const int __incX, const void *__beta, void *__Y, const int __incY)
        ;
void cblas_zgbmv(const enum CBLAS_ORDER __Order,
        const enum CBLAS_TRANSPOSE __TransA, const int __M, const int __N,
        const int __KL, const int __KU, const void *__alpha, const void *__A,
        const int __lda, const void *__X, const int __incX, const void *__beta,
        void *__Y, const int __incY)
                     ;
void cblas_ztrmv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __TransA, const enum CBLAS_DIAG __Diag,
        const int __N, const void *__A, const int __lda, void *__X,
        const int __incX) ;
void cblas_ztbmv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __TransA, const enum CBLAS_DIAG __Diag,
        const int __N, const int __K, const void *__A, const int __lda,
        void *__X, const int __incX)
                     ;
void cblas_ztpmv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __TransA, const enum CBLAS_DIAG __Diag,
        const int __N, const void *__Ap, void *__X, const int __incX)
        ;
void cblas_ztrsv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __TransA, const enum CBLAS_DIAG __Diag,
        const int __N, const void *__A, const int __lda, void *__X,
        const int __incX) ;
void cblas_ztbsv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __TransA, const enum CBLAS_DIAG __Diag,
        const int __N, const int __K, const void *__A, const int __lda,
        void *__X, const int __incX)
                     ;
void cblas_ztpsv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __TransA, const enum CBLAS_DIAG __Diag,
        const int __N, const void *__Ap, void *__X, const int __incX)
        ;
void cblas_ssymv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const int __N, const float __alpha, const float *__A, const int __lda,
        const float *__X, const int __incX, const float __beta, float *__Y,
        const int __incY) ;
void cblas_ssbmv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const int __N, const int __K, const float __alpha, const float *__A,
        const int __lda, const float *__X, const int __incX,
        const float __beta, float *__Y, const int __incY)
        ;
void cblas_sspmv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const int __N, const float __alpha, const float *__Ap,
        const float *__X, const int __incX, const float __beta, float *__Y,
        const int __incY) ;
void cblas_sger(const enum CBLAS_ORDER __Order, const int __M, const int __N,
        const float __alpha, const float *__X, const int __incX,
        const float *__Y, const int __incY, float *__A, const int __lda)
        ;
void cblas_ssyr(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const int __N, const float __alpha, const float *__X, const int __incX,
        float *__A, const int __lda)
                     ;
void cblas_sspr(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const int __N, const float __alpha, const float *__X, const int __incX,
        float *__Ap) ;
void cblas_ssyr2(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const int __N, const float __alpha, const float *__X, const int __incX,
        const float *__Y, const int __incY, float *__A, const int __lda)
        ;
void cblas_sspr2(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const int __N, const float __alpha, const float *__X, const int __incX,
        const float *__Y, const int __incY, float *__A)
        ;
void cblas_dsymv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const int __N, const double __alpha, const double *__A,
        const int __lda, const double *__X, const int __incX,
        const double __beta, double *__Y, const int __incY)
        ;
void cblas_dsbmv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const int __N, const int __K, const double __alpha, const double *__A,
        const int __lda, const double *__X, const int __incX,
        const double __beta, double *__Y, const int __incY)
        ;
void cblas_dspmv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const int __N, const double __alpha, const double *__Ap,
        const double *__X, const int __incX, const double __beta, double *__Y,
        const int __incY) ;
void cblas_dger(const enum CBLAS_ORDER __Order, const int __M, const int __N,
        const double __alpha, const double *__X, const int __incX,
        const double *__Y, const int __incY, double *__A, const int __lda)
        ;
void cblas_dsyr(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const int __N, const double __alpha, const double *__X,
        const int __incX, double *__A, const int __lda)
        ;
void cblas_dspr(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const int __N, const double __alpha, const double *__X,
        const int __incX, double *__Ap)
                     ;
void cblas_dsyr2(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const int __N, const double __alpha, const double *__X,
        const int __incX, const double *__Y, const int __incY, double *__A,
        const int __lda) ;
void cblas_dspr2(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const int __N, const double __alpha, const double *__X,
        const int __incX, const double *__Y, const int __incY, double *__A)
        ;
void cblas_chemv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const int __N, const void *__alpha, const void *__A, const int __lda,
        const void *__X, const int __incX, const void *__beta, void *__Y,
        const int __incY) ;
void cblas_chbmv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const int __N, const int __K, const void *__alpha, const void *__A,
        const int __lda, const void *__X, const int __incX, const void *__beta,
        void *__Y, const int __incY)
                     ;
void cblas_chpmv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const int __N, const void *__alpha, const void *__Ap, const void *__X,
        const int __incX, const void *__beta, void *__Y, const int __incY)
        ;
void cblas_cgeru(const enum CBLAS_ORDER __Order, const int __M, const int __N,
        const void *__alpha, const void *__X, const int __incX,
        const void *__Y, const int __incY, void *__A, const int __lda)
        ;
void cblas_cgerc(const enum CBLAS_ORDER __Order, const int __M, const int __N,
        const void *__alpha, const void *__X, const int __incX,
        const void *__Y, const int __incY, void *__A, const int __lda)
        ;
void cblas_cher(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const int __N, const float __alpha, const void *__X, const int __incX,
        void *__A, const int __lda)
                     ;
void cblas_chpr(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const int __N, const float __alpha, const void *__X, const int __incX,
        void *__A) ;
void cblas_cher2(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const int __N, const void *__alpha, const void *__X, const int __incX,
        const void *__Y, const int __incY, void *__A, const int __lda)
        ;
void cblas_chpr2(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const int __N, const void *__alpha, const void *__X, const int __incX,
        const void *__Y, const int __incY, void *__Ap)
        ;
void cblas_zhemv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const int __N, const void *__alpha, const void *__A, const int __lda,
        const void *__X, const int __incX, const void *__beta, void *__Y,
        const int __incY) ;
void cblas_zhbmv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const int __N, const int __K, const void *__alpha, const void *__A,
        const int __lda, const void *__X, const int __incX, const void *__beta,
        void *__Y, const int __incY)
                     ;
void cblas_zhpmv(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const int __N, const void *__alpha, const void *__Ap, const void *__X,
        const int __incX, const void *__beta, void *__Y, const int __incY)
        ;
void cblas_zgeru(const enum CBLAS_ORDER __Order, const int __M, const int __N,
        const void *__alpha, const void *__X, const int __incX,
        const void *__Y, const int __incY, void *__A, const int __lda)
        ;
void cblas_zgerc(const enum CBLAS_ORDER __Order, const int __M, const int __N,
        const void *__alpha, const void *__X, const int __incX,
        const void *__Y, const int __incY, void *__A, const int __lda)
        ;
void cblas_zher(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const int __N, const double __alpha, const void *__X, const int __incX,
        void *__A, const int __lda)
                     ;
void cblas_zhpr(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const int __N, const double __alpha, const void *__X, const int __incX,
        void *__A) ;
void cblas_zher2(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const int __N, const void *__alpha, const void *__X, const int __incX,
        const void *__Y, const int __incY, void *__A, const int __lda)
        ;
void cblas_zhpr2(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const int __N, const void *__alpha, const void *__X, const int __incX,
        const void *__Y, const int __incY, void *__Ap)
        ;
void cblas_sgemm(const enum CBLAS_ORDER __Order,
        const enum CBLAS_TRANSPOSE __TransA,
        const enum CBLAS_TRANSPOSE __TransB, const int __M, const int __N,
        const int __K, const float __alpha, const float *__A, const int __lda,
        const float *__B, const int __ldb, const float __beta, float *__C,
        const int __ldc) ;
void cblas_ssymm(const enum CBLAS_ORDER __Order, const enum CBLAS_SIDE __Side,
        const enum CBLAS_UPLO __Uplo, const int __M, const int __N,
        const float __alpha, const float *__A, const int __lda,
        const float *__B, const int __ldb, const float __beta, float *__C,
        const int __ldc) ;
void cblas_ssyrk(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __Trans, const int __N, const int __K,
        const float __alpha, const float *__A, const int __lda,
        const float __beta, float *__C, const int __ldc)
        ;
void cblas_ssyr2k(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __Trans, const int __N, const int __K,
        const float __alpha, const float *__A, const int __lda,
        const float *__B, const int __ldb, const float __beta, float *__C,
        const int __ldc) ;
void cblas_strmm(const enum CBLAS_ORDER __Order, const enum CBLAS_SIDE __Side,
        const enum CBLAS_UPLO __Uplo, const enum CBLAS_TRANSPOSE __TransA,
        const enum CBLAS_DIAG __Diag, const int __M, const int __N,
        const float __alpha, const float *__A, const int __lda, float *__B,
        const int __ldb) ;
void cblas_strsm(const enum CBLAS_ORDER __Order, const enum CBLAS_SIDE __Side,
        const enum CBLAS_UPLO __Uplo, const enum CBLAS_TRANSPOSE __TransA,
        const enum CBLAS_DIAG __Diag, const int __M, const int __N,
        const float __alpha, const float *__A, const int __lda, float *__B,
        const int __ldb) ;
void cblas_dgemm(const enum CBLAS_ORDER __Order,
        const enum CBLAS_TRANSPOSE __TransA,
        const enum CBLAS_TRANSPOSE __TransB, const int __M, const int __N,
        const int __K, const double __alpha, const double *__A,
        const int __lda, const double *__B, const int __ldb,
        const double __beta, double *__C, const int __ldc)
        ;
void cblas_dsymm(const enum CBLAS_ORDER __Order, const enum CBLAS_SIDE __Side,
        const enum CBLAS_UPLO __Uplo, const int __M, const int __N,
        const double __alpha, const double *__A, const int __lda,
        const double *__B, const int __ldb, const double __beta, double *__C,
        const int __ldc) ;
void cblas_dsyrk(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __Trans, const int __N, const int __K,
        const double __alpha, const double *__A, const int __lda,
        const double __beta, double *__C, const int __ldc)
        ;
void cblas_dsyr2k(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __Trans, const int __N, const int __K,
        const double __alpha, const double *__A, const int __lda,
        const double *__B, const int __ldb, const double __beta, double *__C,
        const int __ldc) ;
void cblas_dtrmm(const enum CBLAS_ORDER __Order, const enum CBLAS_SIDE __Side,
        const enum CBLAS_UPLO __Uplo, const enum CBLAS_TRANSPOSE __TransA,
        const enum CBLAS_DIAG __Diag, const int __M, const int __N,
        const double __alpha, const double *__A, const int __lda, double *__B,
        const int __ldb) ;
void cblas_dtrsm(const enum CBLAS_ORDER __Order, const enum CBLAS_SIDE __Side,
        const enum CBLAS_UPLO __Uplo, const enum CBLAS_TRANSPOSE __TransA,
        const enum CBLAS_DIAG __Diag, const int __M, const int __N,
        const double __alpha, const double *__A, const int __lda, double *__B,
        const int __ldb) ;
void cblas_cgemm(const enum CBLAS_ORDER __Order,
        const enum CBLAS_TRANSPOSE __TransA,
        const enum CBLAS_TRANSPOSE __TransB, const int __M, const int __N,
        const int __K, const void *__alpha, const void *__A, const int __lda,
        const void *__B, const int __ldb, const void *__beta, void *__C,
        const int __ldc) ;
void cblas_csymm(const enum CBLAS_ORDER __Order, const enum CBLAS_SIDE __Side,
        const enum CBLAS_UPLO __Uplo, const int __M, const int __N,
        const void *__alpha, const void *__A, const int __lda, const void *__B,
        const int __ldb, const void *__beta, void *__C, const int __ldc)
        ;
void cblas_csyrk(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __Trans, const int __N, const int __K,
        const void *__alpha, const void *__A, const int __lda,
        const void *__beta, void *__C, const int __ldc)
        ;
void cblas_csyr2k(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __Trans, const int __N, const int __K,
        const void *__alpha, const void *__A, const int __lda, const void *__B,
        const int __ldb, const void *__beta, void *__C, const int __ldc)
        ;
void cblas_ctrmm(const enum CBLAS_ORDER __Order, const enum CBLAS_SIDE __Side,
        const enum CBLAS_UPLO __Uplo, const enum CBLAS_TRANSPOSE __TransA,
        const enum CBLAS_DIAG __Diag, const int __M, const int __N,
        const void *__alpha, const void *__A, const int __lda, void *__B,
        const int __ldb) ;
void cblas_ctrsm(const enum CBLAS_ORDER __Order, const enum CBLAS_SIDE __Side,
        const enum CBLAS_UPLO __Uplo, const enum CBLAS_TRANSPOSE __TransA,
        const enum CBLAS_DIAG __Diag, const int __M, const int __N,
        const void *__alpha, const void *__A, const int __lda, void *__B,
        const int __ldb) ;
void cblas_zgemm(const enum CBLAS_ORDER __Order,
        const enum CBLAS_TRANSPOSE __TransA,
        const enum CBLAS_TRANSPOSE __TransB, const int __M, const int __N,
        const int __K, const void *__alpha, const void *__A, const int __lda,
        const void *__B, const int __ldb, const void *__beta, void *__C,
        const int __ldc) ;
void cblas_zsymm(const enum CBLAS_ORDER __Order, const enum CBLAS_SIDE __Side,
        const enum CBLAS_UPLO __Uplo, const int __M, const int __N,
        const void *__alpha, const void *__A, const int __lda, const void *__B,
        const int __ldb, const void *__beta, void *__C, const int __ldc)
        ;
void cblas_zsyrk(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __Trans, const int __N, const int __K,
        const void *__alpha, const void *__A, const int __lda,
        const void *__beta, void *__C, const int __ldc)
        ;
void cblas_zsyr2k(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __Trans, const int __N, const int __K,
        const void *__alpha, const void *__A, const int __lda, const void *__B,
        const int __ldb, const void *__beta, void *__C, const int __ldc)
        ;
void cblas_ztrmm(const enum CBLAS_ORDER __Order, const enum CBLAS_SIDE __Side,
        const enum CBLAS_UPLO __Uplo, const enum CBLAS_TRANSPOSE __TransA,
        const enum CBLAS_DIAG __Diag, const int __M, const int __N,
        const void *__alpha, const void *__A, const int __lda, void *__B,
        const int __ldb) ;
void cblas_ztrsm(const enum CBLAS_ORDER __Order, const enum CBLAS_SIDE __Side,
        const enum CBLAS_UPLO __Uplo, const enum CBLAS_TRANSPOSE __TransA,
        const enum CBLAS_DIAG __Diag, const int __M, const int __N,
        const void *__alpha, const void *__A, const int __lda, void *__B,
        const int __ldb) ;
void cblas_chemm(const enum CBLAS_ORDER __Order, const enum CBLAS_SIDE __Side,
        const enum CBLAS_UPLO __Uplo, const int __M, const int __N,
        const void *__alpha, const void *__A, const int __lda, const void *__B,
        const int __ldb, const void *__beta, void *__C, const int __ldc)
        ;
void cblas_cherk(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __Trans, const int __N, const int __K,
        const float __alpha, const void *__A, const int __lda,
        const float __beta, void *__C, const int __ldc)
        ;
void cblas_cher2k(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __Trans, const int __N, const int __K,
        const void *__alpha, const void *__A, const int __lda, const void *__B,
        const int __ldb, const float __beta, void *__C, const int __ldc)
        ;
void cblas_zhemm(const enum CBLAS_ORDER __Order, const enum CBLAS_SIDE __Side,
        const enum CBLAS_UPLO __Uplo, const int __M, const int __N,
        const void *__alpha, const void *__A, const int __lda, const void *__B,
        const int __ldb, const void *__beta, void *__C, const int __ldc)
        ;
void cblas_zherk(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __Trans, const int __N, const int __K,
        const double __alpha, const void *__A, const int __lda,
        const double __beta, void *__C, const int __ldc)
        ;
void cblas_zher2k(const enum CBLAS_ORDER __Order, const enum CBLAS_UPLO __Uplo,
        const enum CBLAS_TRANSPOSE __Trans, const int __N, const int __K,
        const void *__alpha, const void *__A, const int __lda, const void *__B,
        const int __ldb, const double __beta, void *__C, const int __ldc)
        ;
extern void appleblas_sgeadd(const enum CBLAS_ORDER __order,
        const enum CBLAS_TRANSPOSE __transA,
        const enum CBLAS_TRANSPOSE __transB, const int __m, const int __n,
        const float __alpha, const float *__A, const int __lda,
        const float __beta, const float *__B, const int __ldb, float *__C,
        const int __ldc) ;
extern void appleblas_dgeadd(const enum CBLAS_ORDER __order,
        const enum CBLAS_TRANSPOSE __transA,
        const enum CBLAS_TRANSPOSE __transB, const int __m, const int __n,
        const double __alpha, const double *__A, const int __lda,
        const double __beta, const double *__B, const int __ldb, double *__C,
        const int __ldc) ;
extern void ATLU_DestroyThreadMemory()
                     ;
typedef void (*BLASParamErrorProc)(const char *funcName, const char *paramName,
                                   const int *paramPos, const int *paramValue);
void SetBLASParamErrorProc(BLASParamErrorProc __ErrorProc)
        ;
