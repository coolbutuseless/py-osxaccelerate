void vvrecf (float * , const float * , const int * ) ;
void vvrec (double * , const double * , const int * ) ;
void vvdivf (float * , const float * , const float * , const int * ) ;
void vvdiv (double * , const double * , const double * , const int * ) ;
void vvsqrtf (float * , const float * , const int * ) ;
void vvsqrt (double * , const double * , const int * ) ;
void vvcbrtf (float * , const float * , const int * ) ;
void vvcbrt (double * , const double * , const int * ) ;
void vvrsqrtf (float * , const float * , const int * ) ;
void vvrsqrt (double * , const double * , const int * ) ;
void vvexpf (float * , const float * , const int * ) ;
void vvexp (double * , const double * , const int * ) ;
void vvexpm1f (float * , const float * , const int * ) ;
void vvexpm1 (double * , const double * , const int * ) ;
void vvlogf (float * , const float * , const int * ) ;
void vvlog (double * , const double * , const int * ) ;
void vvlog10f (float * , const float * , const int * ) ;
void vvlog10 (double * , const double * , const int * ) ;
void vvlog1pf (float * , const float * , const int * ) ;
void vvlog1p (double * , const double * , const int * ) ;
void vvlogbf (float * , const float * , const int * ) ;
void vvlogb (double * , const double * , const int * ) ;
void vvfabsf (float * , const float * , const int * ) ;
void vvfabs (double * , const double * , const int * ) ;
void vvpowf (float * , const float * , const float * , const int * ) ;
void vvpow (double * , const double * , const double * , const int * ) ;
void vvpowsf (float * , const float * , const float * , const int * ) ;
void vvpows (double * , const double * , const double * , const int * );
void vvsinf (float * , const float * , const int * ) ;
void vvsin (double * , const double * , const int * ) ;
void vvcosf (float * , const float * , const int * ) ;
void vvcos (double * , const double * , const int * ) ;
void vvtanf (float * , const float * , const int * ) ;
void vvtan (double * , const double * , const int * ) ;
void vvasinf (float * , const float * , const int * ) ;
void vvasin (double * , const double * , const int * ) ;
void vvacosf (float * , const float * , const int * ) ;
void vvacos (double * , const double * , const int * ) ;
void vvatanf (float * , const float * , const int * ) ;
void vvatan (double * , const double * , const int * ) ;
void vvatan2f (float * , const float * , const float * , const int * ) ;
void vvatan2 (double * , const double * , const double * , const int * ) ;
void vvsincosf (float * , float * , const float * , const int * ) ;
void vvsincos (double * , double * , const double * , const int * ) ;
void vvsinhf (float * , const float * , const int * ) ;
void vvsinh (double * , const double * , const int * ) ;
void vvcoshf (float * , const float * , const int * ) ;
void vvcosh (double * , const double * , const int * ) ;
void vvtanhf (float * , const float * , const int * ) ;
void vvtanh (double * , const double * , const int * ) ;
void vvasinhf (float * , const float * , const int * ) ;
void vvasinh (double * , const double * , const int * ) ;
void vvacoshf (float * , const float * , const int * ) ;
void vvacosh (double * , const double * , const int * ) ;
void vvatanhf (float * , const float * , const int * ) ;
void vvatanh (double * , const double * , const int * ) ;
void vvintf (float * , const float * , const int * ) ;
void vvint (double * , const double * , const int * ) ;
void vvnintf (float * , const float * , const int * ) ;
void vvnint (double * , const double * , const int * ) ;
void vvceilf (float * , const float * , const int * ) ;
void vvceil (double * , const double * , const int * ) ;
void vvfloorf (float * , const float * , const int * ) ;
void vvfloor (double * , const double * , const int * ) ;
void vvfmodf (float * , const float * , const float * , const int * ) ;
void vvfmod (double * , const double * , const double * , const int * ) ;
void vvremainderf (float * , const float * , const float * , const int * ) ;
void vvremainder (double * , const double * , const double * , const int * ) ;
void vvcopysignf (float * , const float * , const float * , const int * ) ;
void vvcopysign (double * , const double * , const double * , const int * ) ;
void vvnextafterf (float * , const float * , const float * , const int * ) ;
void vvnextafter (double * , const double * , const double * , const int * ) ;
void vvlog2f (float * , const float * , const int * ) ;
void vvlog2 (double * , const double * , const int * ) ;
void vvexp2f (float * , const float * , const int * ) ;
void vvexp2 (double * , const double * , const int * ) ;
void vvsinpif (float * , const float * , const int * ) ;
void vvsinpi (double * , const double * , const int * ) ;
void vvcospif (float * , const float * , const int * ) ;
void vvcospi (double * , const double * , const int * ) ;
void vvtanpif (float * , const float * , const int * ) ;
void vvtanpi (double * , const double * , const int * ) ;
