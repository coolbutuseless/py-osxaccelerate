#!/usr/bin/env python
from __future__ import print_function, division

from libvecLib import libvecLib, ffi

def test_cblas_ddot():
    # Create 2 arrays of length 4
    a = ffi.new("double[]", 4)
    b = ffi.new("double[]", 4)

    for i in range(4):
        a[i] = i
        b[i] = i+1

    print("Array a: ", [a[i] for i in range(4)])
    print("Array b: ", [b[i] for i in range(4)])

    # Accelerate vecLib declaration for dot product of arrays of doubles
    #double cblas_ddot(const int __N, const double *__X, const int __incX, const double *__Y, const int __incY);
    result = libvecLib.cblas_ddot(4, a, 1, b, 1)
    print("Dot product:", result)
    print("Correct result?: ", result==sum(ai * bi for ai, bi in zip(a, b)))
    assert result==sum(ai * bi for ai, bi in zip(a, b))
