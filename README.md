Python CFFI bindings for OSX Accelerate framework
=================================================
This code provides Python [CFFI](http://cffi.readthedocs.org) bindings for some of the libraries available in the **OSX Accelerate** framework.

> The `Accelerate framework` contains C APIs for vector and matrix math, digital signal processing, large number handling, and image processing.

> The Accelerate framework (/System/Library/Frameworks/Accelerate.framework) contains thousands of hand
     tuned high performance library routines for common problems in signal and image processing and general
     and scientific computing.  These routines are provided to help developers and Apple frameworks alike
     make better use of onboard hardware SIMD vector engines (such as SSE and Neon) and multiple processors
     for best performance, without the need to invest in the complexity that SIMD and multithreaded program-ming programming
     ming sometimes requires.

* A nice wordy introduction to OSX Accelerate is provided in the [Accelerate 10.9 docs](https://developer.apple.com/library/mac/documentation/Darwin/Reference/ManPages/man7/Accelerate.7.html)
* [Apple Developer Reference](https://developer.apple.com/library/ios/documentation/Accelerate/Reference/AccelerateFWRef/_index.html#//apple_ref/doc/uid/TP40009465)

Implemented bindings
====================
* [vecLib](https://developer.apple.com/library/mac/documentation/Performance/Conceptual/vecLib/)
    - [BLAS](https://developer.apple.com/library/mac/documentation/Performance/Conceptual/vecLib/)
    - [LAPACK](https://developer.apple.com/library/mac/documentation/Performance/Conceptual/vecLib/)
        - Note: This is a FORTRAN style interface. It is **not** a LAPACKE C Interface.
    - [vectorOps](https://developer.apple.com/library/mac/documentation/Performance/Conceptual/vecLib/#//apple_ref/doc/uid/TP30000414-365541)
        -  vectorOps.h declares a set of vector and matrix BLAS functions on arrays of 128-bit vectors containing single-precision floating-point values. The arrays can be of any desired length, but the number of float elements must be a multiple of 4.

    - [vForce](https://developer.apple.com/library/mac/documentation/Performance/Conceptual/vecLib/#//apple_ref/doc/uid/TP30000414-357225)
        - vForce.h declares a set of trigonometric and transcendental functions in terms of C arrays (double * or float *), which can be of any desired length. Internally, the C arrays are converted piecewise into collections of 128-bit vectors, if appropriate for the current architecture. 

Example
=======
* The code in `test_cblas/test_cblas_ddot()` is a pretty good example of
    - importing `libvecLib`
    - allocating memory for 2 double precision floating point vectors with `cffi` and initialising that memory
    - calling `libveclib.cblas_ddot` to calculate the dot product of the 2 vectors.

Testing
=======
* Use `py.test`
* Tests only cover loading the CFFI binding and using a simple function from the relevant libraries.
* For LAPACK testing
    - an example was copied from [https://software.intel.com/sites/products/documentation/doclib/mkl_sa/11/mkl_lapack_examples/dgels_ex.c.htm](intel)
    - the C version of the test is in the `clapack` directory and should compile using `make`
    - the output of the C version can be compared directly to the output of the `test_clapack.py`

CFFI binding creation
=====================
* The main task in building this binding is in modifying the official C header files so that they can be parsed by CFFI.
* Currently CFFI (v0.8.6) is limited in the parsing of `#define`s and `#if` directives.
* Most of the work in preparing the header files for CFFI can be done by using the `gcc` preprocessor to parse these directives.
* Remaining kruft can be grepped out for the most part.
* Type `make` to parse header files in `apple_headers` into `cffi_headers` for **cblas.h**, **clapack.h** and **vForce.h**
* **vectorOps** was manually modified to be cffi compatible

Bindings ToDo (maybe)
=====================
I was mainly interested in the vector/array operations and have not yet thought about creating CFFI headers for the following parts of the Accelerate Framework:

* [vImage](https://developer.apple.com/library/mac/documentation/Accelerate/Reference/AccelerateFWRef/_index.html#//apple_ref/doc/uid/TP40009465)
    - This framework is designed to provide a suite of image processing primitives. Convolutions, Morphologi-cal Morphological
      cal operators, and Geometric transforms (e.g. scale, shear, warp, rotate) are provided. Alpha composit-ing compositing
      ing and histogram operations are also supported, in addition to various conversion routines between
      different image formats.  vImage uses your image data in place
* [vBigNum](https://developer.apple.com/library/mac/documentation/Performance/Conceptual/vecLib/#//apple_ref/doc/uid/TP30000414-360989)
* [vBasicOps](https://developer.apple.com/library/mac/documentation/Performance/Conceptual/vecLib/#//apple_ref/doc/uid/TP30000414-354495)
    - basic operations on `ints` packed into a 128bit vector
* [vfp](https://developer.apple.com/library/mac/documentation/Performance/Conceptual/vecLib/#//apple_ref/doc/uid/TP30000414-330293)
    - basic operations on `floats` packed into a 128bit vector
* [vDSP](https://developer.apple.com/library/mac/documentation/Performance/Conceptual/vDSP_Programming_Guide/Introduction/Introduction.html#//apple_ref/doc/uid/TP40005147)
    - The vDSP API provides mathematical functions for applications such as speech, sound, audio, and video processing, diagnostic medical imaging, radar signal processing, seismic analysis, and scientific data processing.
    - vectors, convolution, Fourier transforms, signal processing tasks.

Layout of libraries in Accelerate.Framework
===========================================
- /System/Library/Frameworks/Accelerate.framework/Frameworks/vecLib.framework/Versions/Current/
    - libBLAS.dylib
    - libLAPACK.dylib
    - libLinearAlgebra.dylib
    - libvDSP.dylib
    - libvMisc.dylib
    - **vecLib** This library includes all the other dylibs from this directory.
- /System/Library/Frameworks/Accelerate.framework/Frameworks/vImage.framework/Versions/Current/
    - vImage
    - Libraries
        - libCGInterfaces.dylib

Header layout
==============
- /System/Library/Frameworks/Accelerate.framework/Frameworks/vecLib.framework/Headers
    - cblas.h
    - clapack.h
    - LinearAlgebra
        - arithmetic.h
        - base.h
        - linear_systems.h
        - LinearAlgebra.h
        - matrix.h
        - norms.h
        - object.h
        - splat.h
        - vector.h
    - vBasicOps.h
    - vBigNum.h
    - vDSP.h
    - vDSP\_translate.h
    - vecLib.h
    - vecLibTypes.h
    - vectorOps.h
    - vForce.h
    - vfp.h
- /System/Library/Frameworks/Accelerate.framework/Frameworks/vImage.framework/Headers
    - Alpha.h
    - BasicImageTypes.h
    - Conversion.h
    - Convolution.h
    - Geometry.h
    - Histogram.h
    - Morphology.h
    - Transform.h
    - vImage.h
    - vImage\_CVUtilities.h
    - vImage\_Types.h
    - vImage\_Utilities.h 
