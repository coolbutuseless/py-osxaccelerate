all: cblas clapack vForce

cblas:
	# parse header using actual 'gcc' (not clang)
	# remove comment lines (^#) and blank lines (^$) and ignore the 1 line specifying atttribute
	gcc -E apple_headers/vecLib/cblas.h | grep -v "^#" | grep -v "^$$" | grep -v "__attribute" > cffi_headers/vecLib/cblas.h

clapack:
	# parse header using actual 'gcc' (not clang)
	# remove comment lines (^#) and blank lines (^$) 
	gcc -E apple_headers/vecLib/clapack.h | grep -v "^#" | grep -v "^$$" > cffi_headers/vecLib/clapack.h

vForce:
	# parse header using actual 'gcc' (not clang)
	# remove comment lines (^#) and blank lines (^$) and ignore the 1 line specifying atttribute depreacted.
	# Also ignore functions involving complex types
	gcc -E apple_headers/vecLib/vForce.h | grep -v "^#" | grep -v "^$$" | grep "vv" |\
		grep -v "__attribute" | grep -v "complex_t" > cffi_headers/vecLib/vForce.h

