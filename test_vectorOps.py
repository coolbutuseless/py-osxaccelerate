#!/usr/bin/env python
from __future__ import print_function, division

from libvecLib import ffi, libvecLib

def test_vectorOps_vSscal():
    # https://developer.apple.com/library/mac/documentation/Performance/Conceptual/vecLib/#//apple_ref/c/func/vSscal
    # vSscal scales a vector of floats in-place

    # Allocate and initialise array
    N = 32
    a = ffi.new("float[]", N)
    for i in range(N):
        a[i] = i
    print("A in ", [x for x in a])

    # Scale array by 2
    libvecLib.vSscal(N, 2.0, a)
    print("A out", [x for x in a])

    # assert output array matches expected answer
    expected = [2*i for i in range(N)]
    assert all(abs(a-e) < 1e-6 for a,e in zip(a, expected))
